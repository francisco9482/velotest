# Velotest

Speedy

Test de Velocidad de Internet

¿Qué es un [SpeedTest](https://bitcu.co/velocidad-internet/)?

El ancho de banda de internet es la cantidad de datos expresados en Mbps o Gbps que se transfieren en la red.

Existen muchas formas de calcular el speedtest de tu servicio doméstico de internet. El más común es a través del cálculo de la transferencia de datos de subida (upload) y de bajada (download). Estos datos se calculan a través de la transferencia de un archivo de peso suficiente, y calculando el tiempo de subida y de bajada desde el servidor al cliente y viceversa.

¿Qué es un buen ancho de banda?

La definición de un ancho de banda bueno varía de acuerdo a las necesidades tuyas, por ejemplo un ancho de banda de 2 Mbps podría ser bueno para un sistema de envío y recepción de fax, sin embargo podría ser un problema si necesitamos transferir archivos de alta calidad o descargar videos en calidad HD. Para ello tendrás que buscar un servicio de internet que esté de adecuado a tus necesidades.

Upload y Download Simétrico o Asimétrico

El Upload consiste en el tiempo en la taza de transferencia de datos de subida, y por el contrario el download es la taza de transferencia de datos de bajada. Si ambos valores coinciden, se considera que la transferencia de datos es simétrica, es decir, que para ambas direcciones la taza es similar. Si alguno de los valores es inferior, la transferencia es asimétrica. La mayoría de las redes domésticas son asimétricas, dependiendo del servicio de internet del usuario y de su conexión a la línea telefónica DSL o vía satelital. El ancho de banda también varía de acuerdo a la tecnología de cableado, es inferior utilizando Cat-5, después Cat-6 y mucho mejor en tecnología de fibra óptica.

También es muy importante una buena taza de transferencia de datos si utilizas servicios de películas como [mejortorrent1](https://bitcu.co/mejortorrent/) o [dontorrents](https://bitcu.co/dontorrent/).

Calcular el SpeedTest

Existen en la actualidad varias aplicaciones para el cálculo del SpeedTest. Algunas de ellas se encuentran en forma de widgets, que son utilizables directamente por el usuario final desde su navegador de internet, en donde el procedimiento, aún cuando se hace la transferencia de un archivo, éste es eliminado al final del proceso, sin dejar rastro para el usuario que realiza la prueba para la conexión de su servicio.
